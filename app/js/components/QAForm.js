var React = require('react');

/**
 *问题提交表单
 */
module.exports = React.createClass({

    handleForm: function (e) {
        //新版本使用
        e.preventDefault();
        //防止脏数据
        // if(!this.refs.title.getDOMNode().value) return;
        if(!this.refs.title.value) return;

        var newQuestion = {
            // title: this.refs.title.getDOMNode().value,
            title: this.refs.title.value,
            desc: this.refs.desc.value,
            voteCount: 0,
        };
        //提交表单，重置表单
        this.refs.addQaFrom.reset();

        //    因是离线数据。本地内存维持这个数组；
        this.props.onNewQuestion(newQuestion);
    },

    render: function () {
        //组装样式；
        var styleObj = {
            display: this.props.formDisplayed ? 'block' : 'none',
        };

        return (
            <form ref="addQaFrom" style={styleObj} onSubmit={this.handleForm} name="addQuestion" className="clearfix">
                <div className="form-group">
                    {/*<label for="qtitle">问题</label>*/}
                    <label htmlFor="qtitle">问题</label>
                    <input ref="title" type="text" className="form-control" id="qtitle" placeholder="您的问题的标题"/>
                </div>
                <textarea ref="desc" className="form-control" rows="3" placeholder="问题的描述"/>
                <button className="btn btn-success pull-right">确认</button>
                <button onClick={this.props.onToggleForm} className="btn btn-default pull-right">取消</button>
            </form>
        );
    }
});