var React = require('react');

/**
 *问题Item
 */
module.exports = React.createClass({
    /**
     * 加票
     */
    voteUp:function () {
        var newCount = parseInt(this.props.voteCount,10)+1;
        /*this.props.onVote(this._currentElement.key, newCount)*/
        this.props.onVote(this.props.questionKey, newCount)
    },
    /**
     * 减票
     */
    voteDown:function () {
        var newCount = parseInt(this.props.voteCount,10)-1;
        /*this.props.onVote(this._currentElement.key, newCount)*/
        this.props.onVote(this.props.questionKey, newCount)
    },


    render: function () {
        return (
        <div className="media">
            {/*这其父组件中处理了key,不用在这里处理*/}
            {/*<div className="media" key={this.props.key}>*/}
            <div className="media-left">
                <button onClick={this.voteUp} className="btn btn-default">
                    <span className="glyphicon glyphicon-chevron-up"/>
                    <span className="vote-count">{this.props.voteCount}</span>
                </button>
                <button onClick={this.voteDown} className="btn btn-default">
                    <span className="glyphicon glyphicon-chevron-down"/>
                </button>
            </div>
            <div className="media-body">
                <h4 className="media-heading">{this.props.title}</h4>
                <p>{this.props.desc}</p>
            </div>
        </div>
        );
    }
});





