var React = require('react');
var ShowAddButton = require('./ShowAddButton.js');
var QAForm = require('./QAForm.js');
var QAList = require('./QAList.js');
//odash 是一个 JavaScript 实用工具库，提供一致性，模块化，性能和配件等功能。
var _ = require('lodash');

/**
 * 最外层组件
 */
/*var QuestionApp = React.createClass({
 推荐这种方式：*/
module.exports = React.createClass({

    getInitialState: function () {
        var questions = [
            {
                key: 1,
                title: '产品经理与程序员矛盾的本质是什么？',
                desc: '理性探讨，请勿撕逼。产品经理的主要工作职责是产品设计。接受来自其他部门的需求，经过设计后交付研发。但这里有好些职责不清楚的地方。',
                voteCount: 22,
            },
            {
                key: 2,
                title: '热爱编程是一种怎样的体验？',
                desc: '别人对玩游戏感兴趣，我对写代码、看技术文章感兴趣；把泡github、stackoverflow、v2ex、reddit、csdn当做是兴趣爱好；遇到重复的工作，总想着能不能通过程序实现自动化；不给工资我也会来加班；做梦都在写代码。',
                voteCount: 12,
            },
        ];
        //return一定要{}；
        return {
            questions: questions,
            formDisplayed: false,
        };
    },
    /**
     * 数组位置数据添加;更新数据
     * */
    onNewQuestion: function (newQuestion) {
        newQuestion.key = this.state.questions.length + 1;
        //数组添加model数据
        var newQuestions = this.state.questions.concat(newQuestion);

        newQuestions = this.sortQuestion(newQuestions);
        this.setState({
            questions: newQuestions,
        })
    },
    /**
     * 投票后，看Item，总体排序
     * @param key
     * @param newCount
     */
    onVote:function(key,newCount){
        //Returns the new duplicate-value-free array.去重复
        var questions = _.uniq( this.state.questions );
        //(string|undefined): Returns the key of the matched element, else undefined.
        var index = _.findIndex( questions, function(qst){
            return qst.key == key;
        } );
        questions[index].voteCount = newCount;
        questions = this.sortQuestion(questions);
        this.setState({
            questions: questions
        })
    },

    /**
     *数组排序
     */
    sortQuestion: function (questions) {
        questions.sort(function (a, b) {
            return b.voteCount - a.voteCount;
        });
        return questions;
    },


    onToggleForm: function () {
        this.setState({
            formDisplayed: !this.state.formDisplayed,
        });
    },

    render: function () {
        return (
            <div>
                <div className="jumbotron text-center">
                    <div className="container">
                        <h1>React问答</h1>
                        {/*因控件已经封装，不能用原来的onClick={***},得通过回调处理 ,有两个地方，提添加和取消按钮*/}
                        {/* <ShowAddButton onClick={this.onToggleForm}/>*/}
                        <ShowAddButton onToggleForm={this.onToggleForm}/>
                    </div>
                </div>
                <div className="main container">
                    {/*<QAForm formDisplayed = {this.state.formDisplayed}/>*/}
                    <QAForm
                        formDisplayed={this.state.formDisplayed}
                        onToggleForm={this.onToggleForm}
                        onNewQuestion={this.onNewQuestion}
                    />
                    <QAList
                        questions={this.state.questions}
                        onVote = {this.onVote}
                    />
                </div>
            </div>
        );
    }
});
