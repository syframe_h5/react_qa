var React = require('react');
var QAItem = require('./QAItem.js');

/**
 *问题列表
 */
module.exports = React.createClass({

    render:function () {
        var questions = this.props.questions;
        /**
         * 方式一：
         */
      /*    //判断传值是否是数组
        if (!Array.isArray(questions)){
            throw  new Error('this.props.questions问题必须是数组');
        }
        //数组数据分别应该有Item
        var questionComps = questions.map(function (questionComp) {
           // 注意这里的key,不能传递,还有bind(this)绑定
           return <QAItem key={questionComp.key}
                    questionKey = {questionComp.key}
                    title = {questionComp.title}
                    desc = {questionComp.desc}
                    voteCount = {questionComp.voteCount}
            />;
        }.bind(this));*/

        /**
         * 方式二：
         */
        var rows = [];

        //for循环
        questions.forEach(function (question) {
            rows.push(<QAItem key={question.key}
                              questionKey = {question.key}
                              title = {question.title}
                              desc = {question.desc}
                              voteCount = {question.voteCount}
                              //每一个问题的票数；onVote
                              onVote = {this.props.onVote}
            />);
        }.bind(this));

        
        return(
            <div id="questions" className="">
                {/*<QAItem/>*/}
                {/*{questionComps}*/}
                {rows}
            </div>
        );
    }
});