var gulp = require('gulp'),
    connect = require('gulp-connect'),
    browserify = require('gulp-browserify'),
    concat = require('gulp-concat'),
    port = process.env.port || 5000;

//通过browserify管理依赖
gulp.task('browserify', function () {
    //入口点,app.js
    gulp.src('./app/js/main.js')
        .pipe(browserify({
            //利用reactify工具将jsx转换为js
            transform: 'reactify',
        }))
        //输出到js文件夹中
        .pipe(gulp.dest('./dist/js'));

    /* vinyl-source-stream工具
     //转换为gulp能识别的流
     .bundle()
     //合并输出为app.js
     .pipe(source("app.js"))
     .pipe(gulp.dest('./dist/js'));
     */
});

// live reload
gulp.task('connect', function () {
    connect.server({
        // root:'./',
        port: port,
        livereload: true,
    })
})

// reload Js
gulp.task('js', function () {
    gulp.src('./dist/**/*.js')
        .pipe(connect.reload())
})

// reload Html
gulp.task('html', function () {
    gulp.src('./app/**/*.html')
        .pipe(connect.reload())
});


gulp.task('watch', function () {
    gulp.watch('./dist/**/*.js', ['js']);
    gulp.watch('./app/**/*.html', ['html']);
    gulp.watch('./app/js/**/*.js', ['browserify']);
})

//gulp默认命令
gulp.task('default', ['browserify']);
//启动任务serve;从左到右依赖执行
gulp.task('serve', ['browserify', 'connect', 'watch']);
